# Application Modernization - Anthos Workshop

[[_TOC_]]

## Architecture

```mermaid
%%{init: { 'theme': 'default' } }%%
graph TD
classDef dev fill:#F2ECE8,stroke:#333,stroke-width:1px;
classDef stage fill:#99C4C8,color:#fff,stroke:#333,stroke-width:1px;
classDef prod fill:#C3E5E9,stroke:#333,stroke-width:1px;
classDef anthos fill:#E7ECEF,stroke:#333,stroke-width:1px;

subgraph Prod
  subgraph prodgcp[GCP]
    prodgcpvpc[VPC] --> prodgcpgke1[GKE1]
    prodgcpvpc --> prodgcpgke2[GKE2]
  end
  subgraph prodaws[AWS]
    prodawsvpc[VPC] --> prodawseks1[EKS1]
    prodawsvpc --> prodawseks2[EKS2]
  end
  subgraph prodanthos[Anthos]
    prodgcpgke1 -.-> prodasm[ASM]
    prodgcpgke2 -.-> prodasm
    prodawseks1 -.-> prodasm
    prodawseks2 -.-> prodasm
    prodacm[ACM]
  end
end

subgraph Gitlab
  subgraph platform-admins-group[platform-admins group]
    acmrepo[config]
    sharedcdrepo[shared-cd]
  end
  subgraph online-boutique-group[online-boutique group]
    obrepo[online-boutique]
  end
  subgraph bank-of-anthos-group[bank-of-anthos group]
    boarepo[bank-of-anthos]
  end
end
prodacm -.-> acmrepo

class Prod,platform-admins-group,online-boutique-group,bank-of-anthos-group prod;
```

## Introduction to Anthos

[Anthos](https://cloud.google.com/anthos) is a hybrid and multi-cloud platform that allows enterprises to build, deliver and manage the life cycle of modern applications in a fast, scalable, reliable and secure way. In addition to [modern applications](https://cloud.google.com/solutions/cloud-native-app-development), Anthos also integrates to existing applications and application infrastructure which allows enterprises to modernize in place, in the cloud and at their own pace. Anthos platform is cloud agnostic. It can run in an on-premises data center, GCP or any cloud environment. Anthos platform is composed of a number of components that provide the following functionality:

- Container management (via [Anthos GKE](https://cloud.google.com/anthos/gke) or [Anthos attached clusters](https://cloud.google.com/anthos/docs/setup/attached-clusters))
- Policy management and enforcement (via [Anthos Configuration Management](https://cloud.google.com/anthos/config-management))
- Services management (via [Anthos Service Mesh](https://cloud.google.com/anthos/service-mesh))
- Application and software life cycle management ([CI/CD](https://cloud.google.com/solutions/modern-ci-cd-with-anthos))
- Observability and platform management (via [Cloud Operations](https://cloud.google.com/products/operations))

## About the workshop

When your Kubernetes cluster(s) have grown large or when you’re looking into using more than one computing location, think Anthos. Hybrid- (on-prem and in the cloud) and multi-cloud is a necessary reality for many organizations. Whether they’re different departments using their preferred tooling, or whether they would like to keep some data on-prem while moving other data to the cloud, Anthos will reduce complexity. It provides a “single pane of glass” where network and security administrators can visualize and control hybrid-cloud deployments.

This workshop is the culmination of the previous workshops. This will combine your knowledge of Docker, Kubernetes, Istio, GitOps, and CI/CD to paint a picture of massive-scale deployments. Come and see how Google manages its vast number of Kubernetes clusters and billions of containers while keeping the process manageable and cost effective. 

### Learning Outcomes
* What is Anthos
* What problems does Anthos solve
* Hybrid/Multi-cloud computing
* Secure Policy Management
* Company-wide security policy enforcement
* Simplified patching
* Managed with GitOps
* Multi-Cloud Service Mesh
* Company-wide visibility and tracing
* Simplify multi-cloud deployments

This lab is a vastly scaled down version of the work Ameer and Ken did [this](https://gitlab.com/anthos-multicloud/anthos-multicloud-workshop). I've tried to minimize changes to their codebase, modifying only that which isn't needed for an intro to Anthos. For example, making the deployment single env, using only BoA, and of course only having one session. This labe is meant to cover Anthos at a high level. In order to defer discussion on friction points around the use of GSCR, we will be using Gitlab to facilitate the code repository for this lab as well.


At the end of this workshop we will:
* Create a simple deployment of Anthos in AWS and in GKE. 
* Leverage ACM to create guardrails around our environments
* Investigate ASM 
* Set up simple pipelines to run and deploy our code

For a fuller look at the platform I urger you to dive into the Multi-cloud workshop listed above. 
Additionally we make heavy use of the [Bank of Anthos](https://github.com/GoogleCloudPlatform/bank-of-anthos) code base to make this workshop a success. That said this approach is far simpler in it's goals:



Friction points
* Cloud build deploying into AWS
* Keys in AWS
* Access to GSCR


There are two sections to this workshop:
1. [**Infrastructure**](/README.md#infrasructure) - Installation of the infrastructure necessary for this workshop
1. [**Application**](/README.md#application) - Using the Anthos Infrastructure



## Infrastructure - Installing Anthos platform


To set up the Anthos platform foundation, you accomplish the following:

- Setting up an Anthos multi-cloud environment on GCP and AWS using GKE and EKS (Anthos attached clusters, registered via Hub).
- Setting up VPCs in each environment in GCP and VPC.
- Setting up GKE and EKS clusters in GCP and AWS.
- Deploying Anthos Config Management (ACM) on all clusters.
- Setting up Google Code Source Repository for source code management (SCM) with repos.
- Creating multicloud service meshes using Anthos Service Mesh (ASM)

## Setting up the environment in Qwiklabs

This workshop is intended to be run in [Qwiklabs](https://explore.qwiklabs.com). You should have been invited to a Qwiklabs "Classroom". In the "Classrooms" icon on the left of the home screen, navigate to the 'Anthos Multicloud Workshop GCP' classroom.

You will now see two labs in Qwiklabs as part of this workshop. One of the labs sets up an environment in GCP (a clean GCP project) and the other sets up an environment in AWS (a federated qwiklabs managed account). Starting both of these labs provide you with credentials to both environments.

For GCP, you get a Google account username, password and a GCP project. You use these credentials to access and administer resources in your provided GCP project via GCP Console and Cloud Shell.

- Tip: You can open an incognito window in Chrome to log in as this temporary user, without having to deal with switching Google accounts.
- Once you log in using the user/pass in the qwiklab window, you should see the GCP console, logged into project: `qwiklabs-gcp-1234abcd`
- You should also see at the top right that you're logged in as a User, like: `student-00-1234abcd@qwiklabs.net`

For AWS, you get an Access Key ID and a Secret Access Key.  These credentials allow you full control.

- Tip: No need to open this in an incognito window. Instead, just click the "Open Console" button.
- On the first try, this will fail :( -- it is creating your credentials and you'll see a banner: `Creating login credentials. Please try again in 30 seconds.`
- Try again, and you should be logged into https://console.aws.amazon.com/console/home?region=us-east-1#
- You should see that you're logged in at the top right: `awsstudent @ [1234-1234-1234]`

These two environments are temporary and expire at the end of this workshop (or when time expires). If you would like a persistent setup of this workshop, you can follow the same instructions using your own GCP and AWS accounts.
## Setup

In Qwiklabs, you should see two labs. One lab starts the GCP environment, and the other starts the AWS environment.

- Start both lab environments. Starting the two labs will give you credentials to both GCP and AWS environments.
- From the GCP lab, open Cloud Shell. This lab is intended to be run from Cloud Shell.

```
ssh.cloud.google.com
```

- Set GCP and AWS credentials. Get the value of the GCP Project ID, GCP Username, AWS Access Key ID and AWS Secret Access
  Key from Qwiklabs and replace the values with your values below.

```
export GOOGLE_PROJECT=[GCP PROJECT ID] # example export GOOGLE_PROJECT=qwiklabs-gcp-1234abcd
export GCLOUD_USER=[GCP USERNAME] # example export GCLOUD_USER=student-123@qwiklabs.net
export AWS_ACCESS_KEY_ID=[AWS_ACCESS_KEY_ID] # example export AWS_ACCESS_KEY_ID=ABCDEF123456
export AWS_SECRET_ACCESS_KEY=[AWS_SECRET_ACCESS_KEY] # example export AWS_SECRET_ACCESS_KEY=123abc456def789def
```

- Create a `WORKDIR` for this tutorial. All files related to this tutorial end up in `WORKDIR`.

```
mkdir -p $HOME/appmod-anthos && cd $HOME/appmod-anthos && export WORKDIR=$HOME/appmod-anthos
```

- Clone the workshop repo.

```
git clone https://gitlab.com/jasonnic/appmod-anthos-workshop ${WORKDIR}/appmod-anthos-workshop
```

## Deploying the environment

- Run the `build.sh` script from the root folder to set up the environment in GCP and AWS. The `build.sh` script runs the [`bootstrap.sh`](platform_admins/scripts/bootstrap.sh) script, for more information about the script see the [bootstrap.sh documentations](platform_admins/docs/scripts.md#bootstrapsh)

```
cd ${WORKDIR}/anthos-multicloud-workshop
gcloud config set project ${GOOGLE_PROJECT}

gcloud auth login
<Enter verification code>

gcloud auth list
<Authorize gcloud>

./build.sh
```

> Note that the infrastructure build process can take approximately 30 - 35 minutes to complete.

- After the `build.sh` script finishes, navigate to the **Cloud Build** details page in Cloud Console from the left hand navbar.
- Initially, you see the `main` build running. Click on the build ID to inspect the stages of the pipeline. The `main` build pipeline trigger additional builds.
- The following diagram illustrates the builds and the approximate times each stage takes to complete. Note that all the `env` and the `gitlab` stages run concurrently as shown.

```mermaid
%%{init: { 'theme': 'default' } }%%
graph LR
classDef pipeline fill:#FAFAC6,stroke:#333;

commit[commit to main] -->|6mins|Main[Main - Create custom builder] -->|2mins| Project_Setup[Project Setup - SSH Keys and GCP Service Accounts]
Project_Setup -->|26mins| Prod[Prod Pipeline - Sets up the prod environment]
Project_Setup -->|20mins| Gitlab[Gitlab and Repos - Sets up Gitlab and repos]

class Main,Project_Setup,Prod,Gitlab pipeline;
```

- You can trigger this pipeline by running the `build.sh` script which commits the changes to the `infrastructure` CSR repo's _main_ branch.
- Alternatively, you can directly commit changes to the `infrastructure` repo which is cloned in the `${WORKDIR}/infra-repo` folder in Cloud Shell.
  > Running the `build.sh` script overrides any changes you make locally through the `infra-repo` folder.

## Infrastructure Pipeline

The following illustration provides a detailed view of the pipelines and the resources that are created, for more information see the [infrastructure pipeline documentation](platform_admins/docs/pipelines.md#infrastructure-pipeline).

```mermaid
%%{init: { 'theme': 'default' } }%%
graph LR
classDef aws fill:#F2ECE8,stroke:#333,stroke-width:2px;
classDef gcp fill:#99C4C8,stroke:#333,stroke-width:2px;
classDef mesh fill:#C3E5E9,stroke:#333,stroke-width:1px;
classDef anthos fill:#FAFAC6,stroke:#333,stroke-width:1px,stroke-dasharray: 5 5;

installer([Build Installer Image])

prodgcpvpc([Prod GCP VPC])
prodgke([2 x Prod GKE w/ ACM])


gitlab([Gitlab])
repos([Repos])
hub_gsa([Anthos Hub GCP SA])
cloudops_gsa([Cloud Ops GCP SA])
cicd_gsa([CICD GCP SA])
cnrm_gsa([Config Connector GCP SA])
gcr([Google Container Registry])
kcc([Anthos Config Connector])
autoneg([GKE Autoneg Controller])
ssh_key([SSH Key Pair for ACM and all repos])

prodawsvpc([Prod AWS VPC])
prodeks([2 x Prod EKS w/ ACM])

prodasm[Prod ASM]

ssh_key --> prodgcpvpc
ssh_key --> prodawsvpc

ssh_key --> gitlab -->|Create repos| repos

installer -->|Register Anthos clusters to Hub and store creds in GCS| hub_gsa
installer -->|For non-GCP clusters to send metrics to Cloud Ops and store creds in GCS| cloudops_gsa
installer -->|For CICD runners to deploy applications and store creds in GCS| cicd_gsa
installer -->|For Anthos Config Connector and store creds in GCS| cnrm_gsa
installer -->|Make GCR public for CICD runners to use installer image| gcr
installer -->|Create SSH key pair for ACM and all repos and store in GCS| ssh_key

ssh_key -.->|Public key as deploy token| repos

%%hub_gsa -.-> prodeks

subgraph Gitlab_Pipeline
  gitlab
  repos
end

subgraph Project_Pipeline
  ssh_key
  gcr
  subgraph GCP Service Accounts
    hub_gsa
    cloudops_gsa
    cicd_gsa
    cnrm_gsa
  end
end

subgraph Main_Pipeline
  installer
end

subgraph Prod_Pipeline
  subgraph Prod
    subgraph GCP_Prod[GCP]
      prodgcpvpc --> prodgke
      prodgke --> kcc
      prodgke --> autoneg
    end
    subgraph AWS_Prod[AWS]
      prodawsvpc --> prodeks
    end
    subgraph ASM_Prod[ASM]
      prodgke -.-> prodasm
      prodeks -.-> prodasm
    end
  end
end

class AWS_Prod aws;
class GCP_Prod gcp;
class ASM_Prod mesh;
class Prod_Pipeline,Gitlab_Pipeline,Project_Pipeline,Main_Pipeline anthos;
```

## User setup

- Verify that all pipelines finish successfully.

```bash
gcloud builds list
```

optional: follow progress of builds with

```bash
watch -n 10 gcloud builds list
```

_OUTPUT (Do not copy)_

```bash
ID                                    CREATE_TIME                DURATION  SOURCE                                                                                                      IMAGES  STATUS
ca7c4cd3-cd26-46ab-b1ba-49d93e80d567  2020-09-11T21:23:49+00:00  24M23S    gs://qwiklabs-gcp-02-3d346cf87fd8_cloudbuild/source/1599859427.742461-84b48dc9b6c94fe1b1d9f4f3c490f635.tgz  -       SUCCESS
a04832fd-df8a-4533-9307-ff5ee39e813d  2020-09-11T21:23:48+00:00  17M50S    gs://qwiklabs-gcp-02-3d346cf87fd8_cloudbuild/source/1599859427.061173-61bcedfeae134da7b4783afde66a3ead.tgz  -       SUCCESS
52712167-d29f-47f9-b6bc-f530efac3bee  2020-09-11T21:23:47+00:00  22M9S     gs://qwiklabs-gcp-02-3d346cf87fd8_cloudbuild/source/1599859425.499421-2e5ee25907124f02b0822ee12f3d03e2.tgz  -       SUCCESS
4a7d7686-46a3-440a-af46-1be607205b16  2020-09-11T21:23:46+00:00  6M53S     gs://qwiklabs-gcp-02-3d346cf87fd8_cloudbuild/source/1599859425.402949-c875d1b65f7a49dcacde5d766fe7ac62.tgz  -       SUCCESS
1aeab461-7b31-4ae3-bfac-9700c18f9819  2020-09-11T21:21:37+00:00  2M3S      gs://qwiklabs-gcp-02-3d346cf87fd8_cloudbuild/source/1599859295.678195-ef981107e3e3465daa3d54a54a9c7f05.tgz  -       SUCCESS
ed662a31-cc37-4411-9eb3-87425757557a  2020-09-11T21:17:48+00:00  6M2S      infrastructure@bba1e6558c8da62e211870a09c9fb05594687081                                                     -       SUCCESS
```

- You can also view this through the **Cloudbuild** page in Cloud Console.

<img src="/platform_admins/docs/img/cloudbuild_success.png" width=70% height=70%>

- Run the `user_setup.sh` script from the repository root folder.

```bash
source ${HOME}/.bashrc # If you're using ZSH, source ${HOME}/.zshrc
cd ${WORKDIR}/anthos-multicloud-workshop
source ./user_setup.sh
```

- This `user_setup.sh` script performs the following steps:
  - Downloads EKS cluster _kubeconfig_ files. The location of these files is in the `${WORKDIR}/kubeconfig` folder.
  - Downloads SSH-Key pair. SSH Keys are used to interact with Gitlab repos. The location of these files is in the `${WORKDIR}/ssh-keys` folder.
  - Downloads the Gitlab hostname and root password txt file. The location of the file is in the `${WORKDIR}/gitlab` folder.
  - Creates a combined _kubeconfig_ file with all cluster contexts. Renames the clusters for easy context switching. The location of the merged _kubeconfig_ file is `${WORKDIR}/kubeconfig/workshop-config`. The script also sets this as your `KUBECONFIG` variable.
  - Get the EKS cluster's Kubernetes Service Account tokens to login to through the Cloud Console. Learn about logging in to Anthos registered clusters [here](https://cloud.google.com/anthos/multicluster-management/console/logging-in).
    > The script is idempotent and can be run multiple times.

_OUTPUT from the `user_setup.sh` script (Do not copy)_

```bash
*** eks-prod-us-west2ab-1 Token ***

[EKS Cluster Token]

*** eks-prod-us-west2ab-2 Token ***

[EKS Cluster Token]

*** eks-stage-us-east1ab-1 Token ***

[EKS Cluster Token]

*** Gitlab Hostname and root password ***

gitlab.endpoints.PROJECT_ID.cloud.goog
[`root` PASSWORD]
```

### Variables

The initial `build.sh` script creates a `vars.sh` file in the `${WORKDIR}` folder. The `user_setup.sh` script adds additional variables for the workshop to the `vars.sh` file. The `vars.sh` file is automatically sourced when you log in to Cloud Shell. You can also manually source the file.

### Logging in to EKS clusters

There are three EKS clusters in the architecture. Two clusters in the `prod` environment and one in the `stage` environment. The tokens from the `user_setup.sh` script can be used to log in to the EKS clusters in Cloud Console.

- Navigate to the **Kubernetes Engine > Clusters** page in Cloud Console. You can see the three EKS clusters registered. They have not been logged in.

<img src="/platform_admins/docs/img/eks_loggedout.png">

- Click **Login** next to each EKS cluster and select **Token**. Copy and paste the tokens (outputted from the `user_setup.sh` script) to the EKS clusters.
- Note: Simply select the token in the cloud shell (and nothing else, this automatically copies is), and paste it into the token box in the Cloud Console without intermittently storing it into a file. Any intermittent storage might add extra characters that will invalidate the token and login will fail.

<img src="/platform_admins/docs/img/eks_login.png" width=40% height=40%>

- Navigate to the **Workloads** and **Services** pages and verify you can see metadata information from the EKS clusters. This confirms you have successfully logged in.

<img src="/platform_admins/docs/img/eks_workloads.png" width=80% height=80%>

### Logging in to Gitlab

Log in to Gitlab.

- Navigate to the output link of the following command.

```bash
echo -e "https://gitlab.endpoints.${GOOGLE_PROJECT}.cloud.goog"
```

- Log in with the username `root` and the gitlab password from the `user_setup.sh` script.
- You see multiple projects (or repos).

<img src="/platform_admins/docs/img/gitlab_repos.png" width=50% height=50%>

## Labs

Now that the installation is complete we can start working through the platform. First, lets talk about what we've installed. 

Walk through the compoents of the installation
GKE
EKS
GitLab
Config Repo
CI/CD Platform
Why
Contrast with how it would be done by hand

### Anthos Configuration Management

[Anthos Config Management](https://cloud.google.com/anthos/config-management) uses the `config` repository to deploy Kubernetes manifests to all Kubernetes clusters in the platform. This way, the `config` repo becomes the _source of truth/record_ for all application configuration. The files needed for the repository are located in the `/platform_admins/starter_repos/config` folder of the workshop repository. You copy the contents of this folder into the `config` repo in Gitlab (created in the workshop) to prepare the repo.

`config` repository. This repository is responsible for creating a _landing zone_ for each service to be deployed. The term _landing zone_ refers to a portion of the platform that is configured for a specific service. In Kubernetes, this could be a [namespace](https://kubernetes.io/docs/concepts/overview/working-with-objects/namespaces/) as well as policies that may be required for a particular service. Each service gets deployed in its own namespace. Anthos Config Management [ACM](https://cloud.google.com/anthos/config-management) _config sync_ functionality is used to _pull_ Kubernetes config from the `config` repo and apply to the clusters. This ensures these namespaces are created in all clusters. `config` repo is also used to deploy the final _hydrated_ service configs to the clusters. Hydrated Kubernetes config refers to the final intended state for a service.

@todo 
Explain what the config directory is and what it does

## `config` Repository

1. Run the following commands to initialize the `config` repository.

```bash
source ${WORKDIR}/appmod-anthos-workshop/user_setup.sh
cd ${WORKDIR}
# init git
git config --global user.email "${GCLOUD_USER}"
git config --global user.name "Cloud Shell"
if [ ! -d ${HOME}/.ssh ]; then
  mkdir ${HOME}/.ssh
  chmod 700 ${HOME}/.ssh
fi
# pre-grab gitlab public key
ssh-keyscan -t ecdsa-sha2-nistp256 -H gitlab.endpoints.${GOOGLE_PROJECT}.cloud.goog >> ~/.ssh/known_hosts
git clone git@gitlab.endpoints.${GOOGLE_PROJECT}.cloud.goog:platform-admins/config.git
cd config
cp -r ${WORKDIR}/appmod-anthos-workshop/platform_admins/starter_repos/config/. .
git add .
git commit -m "initial commit"
git branch -m master main
git push -u origin main
```

1. Wait a few moments and ensure all clusters (except `gitlab` cluster) are `SYNCED` to the `config` repo. Run the following command.

   - if you're having trouble with git and ssh-keys, make sure your ssh-agent is running. the easiest thing to do is to re-run the setup script via: `source ${WORKDIR}/appmod-anthos-workshop/user_setup.sh` (note the prefix `source `)
   
```bash
watch nomos status
```

You may need to run this command multiple times until clusters are synced. The output should look as follows.

(Command Output)
```
Current   Context                                              Sync Status      Last Synced Token   Sync Branch   Resource Status
-------   -------                                              -----------      -----------------   -----------   ---------------
          eks_eks-stage-us-east1ab-1                           SYNCED           93ffc3be            main          Healthy
*         ke_qwiklabs-gcp-02-e8d824be38ef_us-central1_gitlab   NOT INSTALLED
          p-02-e8d824be38ef_us-east4-b_gke-stage-us-east4b-1   SYNCED           93ffc3be            main          Healthy
```

Syncing to the `config` repo ensures that the namespaces for the Online Boutique application are created on all clusters. You can now deploy the Bank of Anthos app.

Live Modification of the config repo

1. Change context to EKS
1. Confirm that the namespaces were created
1. Delete the ob-checkout namespace
1. Watch for changes
1. In the config repo delete the online botique (ob) we won't be using it here.
1. Commit and Push
1. Watch for changes

```
kubectl ctx eks_eks-stage-us-east1ab-1
kubectl get ns
```

(Command Output)
```
ob-ad                      Active   8m43s
ob-cart                    Active   8m44s
ob-checkout                Active   8m41s
ob-currency                Active   8m43s
ob-email                   Active   8m44s
ob-frontend                Active   8m43s
ob-loadgenerator           Active   8m42s
ob-payment                 Active   8m42s
ob-productcatalog          Active   8m43s
ob-recommendation          Active   8m40s
ob-redis                   Active   8m44s
ob-shipping                Active   8m41s
```

```
kubectl delete ns ob-checkout && kubectl get ns --watch
```

(Command Output)
```
namespace "ob-checkout" deleted
...
ob-checkout                Active   0s
ob-currency                Active   10m
ob-email                   Active   10m
ob-frontend                Active   10m
ob-loadgenerator           Active   10m
ob-payment                 Active   10m
ob-productcatalog          Active   10m
ob-recommendation          Active   10m
ob-redis                   Active   10m
ob-shipping                Active   10m
```

Anthos Configuration Management is constantly checking for drift in the clusters it's managing. Within 15-20 seconds it will dectect the change and redeploy the deleted namespace based on the configuration stored in the `config` directory. 

To make the change we are seeking we actually have to delete the ob* directories out of the config repo. Since we are already in the config directory we can make the changes directly.

If you want to confirm the *Last Synced Token* changes, run `git log` and compare the commit tag

```
git log
rm -rf namespaces/online-boutique
git add .
git commit -m "remove the online botique directory"
git push -u origin main
watch nomos status
```

Now ideally this would be wrapped in something like a pre-commit hook, a MR, or a PR. We can also run the `nomos vet` command prior to commit, or as part of a CI process on the config pipeline. The point being that modifications to this repository will impact every single cluster tied to ACM. The good news is beecause we are using source code control, we can roll back to *good* configurations if things go horribly wrong.

(Command Output)
```
Current   Context                                              Sync Status      Last Synced Token   Sync Branch   Resource Status
-------   -------                                              -----------      -----------------   -----------   ---------------
*         eks_eks-stage-us-east1ab-1                           SYNCED           93ffc3be            main          Healthy
          ke_qwiklabs-gcp-02-e8d824be38ef_us-central1_gitlab   NOT INSTALLED
          p-02-e8d824be38ef_us-east4-b_gke-stage-us-east4b-1   SYNCED           93ffc3be            main          Healthy
```

Eventually it should change to:
```
Current   Context                                              Sync Status      Last Synced Token   Sync Branch   Resource Status
-------   -------                                              -----------      -----------------   -----------   ---------------
          eks_eks-prod-us-west2ab-1                            SYNCED           e00d54cc            main          Healthy
          eks_eks-prod-us-west2ab-2                            SYNCED           e00d54cc            main          Healthy
*         ke_qwiklabs-gcp-01-2290b527a1ed_us-central1_gitlab   NOT INSTALLED
          cp-01-2290b527a1ed_us-west2-a_gke-prod-us-west2a-1   SYNCED           e00d54cc            main          Healthy
          cp-01-2290b527a1ed_us-west2-b_gke-prod-us-west2b-2   SYNCED           e00d54cc            main          Healthy
```

Confirming the change to the cluster:

```
kubectl get ns
```

(Command Output)
```
NAME                       STATUS   AGE
boa-accounts-db            Active   37m
boa-balancereader          Active   37m
boa-contacts               Active   37m
boa-frontend               Active   37m
boa-ledger-db              Active   37m
boa-ledgerwriter           Active   37m
boa-loadgenerator          Active   37m
boa-transactionhistory     Active   37m
boa-userservice            Active   37m
config-management-system   Active   40h
db-crdb                    Active   37m
db-redis                   Active   37m
default                    Active   40h
gatekeeper-system          Active   40h
gke-connect                Active   40h
istio-system               Active   40h
kube-node-lease            Active   40h
kube-public                Active   40h
kube-system                Active   40h
```

## Set up Continuous Delivery in GitLab

Along with the profiles, our Anthos DevRel team developed a framework around how best pracitices should be shared between platform admins, and developers. One of thse practices is the use of shared continuous delivery templates to facilitate developer's own pipelines. The next set of steps walk you through creation of the pipelines in the hosted GitLab environment.

Like the other steps we will be taking the code in the `starter_reps` and pushing it up into our GitLab instance. At a high level this directory contains:
* build container image
* build manifests
* create third party state 
* set workload identity
* commit to config management 

```
cd ${WORKDIR}
git clone git@gitlab.endpoints.${GOOGLE_PROJECT}.cloud.goog:platform-admins/shared-cd.git
cd ${WORKDIR}/shared-cd
cp -r ${WORKDIR}/appmod-anthos-workshop/platform_admins/starter_repos/shared_cd/. .
git add .
git commit -m "initial commit"
git branch -m master main
git push -u origin main

```

Unlike our previous examples the new Ban kof Anthos uses Cockroach DB instead of Postgres (explain why) 

1. Create cockroachdb repository
2. Verify Installation
3. Import Test Data

```
cd ${WORKDIR}
git clone git@gitlab.endpoints.${GOOGLE_PROJECT}.cloud.goog:databases/cockroachdb.git
cd ${WORKDIR}/cockroachdb
cp -r ${WORKDIR}/appmod-anthos-workshop/platform_admins/starter_repos/cockroachdb/. .
git add .
git commit -m "initial commit"
git branch -m master main
git push -u origin main
```



### Deploy the Bank of Anthos Application

In this segment we will deploy the Bank of Anthos Application(BoA) to the staging enviroment. BoA is a built on seven microservices with an additional loadgenerator service used to provide testing and telemetry. In this case we will deploy everything to the EKS and the GCP clusters. We can see the source code for our BoA applcation in `./platform_admins/starter_repos/bank_of_anthos` directory. Rather than use a scripted or manual installation of the repository we are going to install the repo leveraging the CI/CD capabilities of GitLab.



We are going to be using the boa_stage.sh script to deploy our application. 

1. Change Directory to `WORKDIR`
1. Source vars
1. Run the Deploy Script
1. Talk about what is getting deployed
1. Check for installation




### Anthos Service Mesh

What is it what can we do with it, SLO, SLI 
Access to Kiali


